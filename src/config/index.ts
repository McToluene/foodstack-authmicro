import dotenv from "dotenv";

process.env.NODE_ENV = process.env.NODE_ENV || "development";
const envFound = dotenv.config();

if (!envFound) {
  throw new Error("Environment file not found!");
}

export default {
  port: parseInt(process.env.PORT, 10),

  //Winston log level
  logs: {
    level: process.env.LOG_LEVEL || "silly",
  },

  // Mongodb connection
  databaseURL: `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,

  // api route prefix
  api: {
    prefix: "/api/v1",
  },
};
