import mongoose from "mongoose";
import { Db } from "mongodb";
import config from "../../config";

export default async (): Promise<Db> => {
  try {
    const options = {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      connectTimeoutMS: 10000,
    };
    const connection = await mongoose.connect(config.databaseURL, options);
    return connection.connection.db;
  } catch (error) {
    throw new Error(error);
  }
};
