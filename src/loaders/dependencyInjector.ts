import { Document } from "mongoose";
import { Container } from "typedi";
import logger from "./logger";

export default async ({ models }: { models: { name: string; model: any & Document }[] }): Promise<void> => {
  try {
    models.forEach((model) => {
      Container.set(model.name, model.model);
    });
    Container.set("logger", logger);
  } catch (error) {
    logger.error("Error on dependency injector loader: %o", error);
    throw new Error(error);
  }
};
