import express, { Request, Response, NextFunction, Application } from "express";
import swaggerUi from "swagger-ui-express";
import config from "../config";
import { swaggerDocument } from "../docs/swagger";
import routes from "../api";

export default async ({ appInstance }: { appInstance: Application }): Promise<void> => {
  /**
   * Health Check endpoints
   * @TODO Explain why they are here
   */
  appInstance.get("/status", (req: Request, res: Response) => {
    res.status(200).json({ message: "Welcome to FOOD-STACK auth microservice" });
  });

  // Middleware that transform the raw req.body to json
  appInstance.use(express.json());
  appInstance.use(express.urlencoded({ extended: true }));

  // Openshif(formerly called Swagger) Documentation
  appInstance.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  // Calling the application routes
  appInstance.use(config.api.prefix, routes());

  // Catch 404 and forward to error handler
  appInstance.use((req: Request, res: Response, next: NextFunction) => {
    const err = new Error("Not Found");
    err["status"] = 404;
    next(err);
  });

  appInstance.use((err: any, req: Request, res: Response, next: NextFunction) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
      },
    });
    next();
  });
};
