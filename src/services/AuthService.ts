import { Service, Inject } from "typedi";
import { Logger } from "winston";

@Service()
export class AuthService {
  constructor(@Inject("logger") private logger: Logger, @Inject("UserModel") private userModel: Models.UserModel) {}

  async verifyNumber(number: string): Promise<{ isExist: boolean }> {
    try {
      let isExist = false;
      const restaurant = await this.userModel.findOne({ phonenumber: number });
      if (restaurant) {
        isExist = true;
      }
      return { isExist };
    } catch (error) {
      this.logger.error(error);
      throw new Error(error);
    }
  }
}
